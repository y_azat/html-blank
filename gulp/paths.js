module.exports = {
    src:   {
        pug:    'src/templates/*.pug',
        js:     'src/js/index.js',
        styles: "src/styles/styles.scss",
        img:    'src/img/**/*',
        assets: 'src/assets'
    },
    build: {
        dist:   'dist',
        assets: 'dist/assets',
        js:     'dist/assets/js',
        css:    'dist/assets/css',
        img:    'dist/assets/img'
    },
    watch: {
        html:  'dist/*.html',
        ejs:   'src/templates/**/*.ejs',
        js:    'src/js/**/*.js',
        style: 'src/styles/**/*.scss',
        img:   'src/img/**/*'
    },
    tmp:   '.tmp'
}