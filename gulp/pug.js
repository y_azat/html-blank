const gulp  = require('gulp'),
      paths = require('./paths'),
      $     = require('gulp-load-plugins')(),
      isDev = require('./env.js');

gulp.task('pug', function buildHTML() {
    return gulp.src(paths.src.pug)
        .pipe($.plumber())
        .pipe($.changed(paths.build.dist, {extension: '.html'}))
        .pipe($.if(isDev, $.cached('pug')))
        .pipe($.pugInheritance({basedir: paths.src.pug, skip: 'node_modules'}))
        .pipe($.filter(function (file) {
            return !/\/_|\\_/.test(file.path) && !/^_/.test(file.relative); // /\/_|\\_/ fix windows path (include backslash)
        }))
        .pipe($.pug({
            pretty:  true,
            basedir: '/'
        }))
        .pipe($.rename(function (path) {
            path.dirname = '';
            return path;
        }))
        .pipe(gulp.dest(paths.build.dist));
});
