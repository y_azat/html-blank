const gulp  = require('gulp'),
      paths = require('./paths'),
      $     = require('gulp-load-plugins')();

gulp.task('assets', function() {
    return gulp.src(paths.src.assets + '**/*.*')
        .pipe($.newer(paths.build.assets))
        .pipe(gulp.dest(paths.build.assets));
});
