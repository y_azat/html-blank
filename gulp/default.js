'use strict';
const gulp = require('gulp'),
      runSequence = require('run-sequence');

gulp.task('default', function(callback) {
    runSequence('clean', ['pug', 'styles', 'js', 'imagemin', 'assets', 'watch', 'serve'])
});