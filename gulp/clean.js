const gulp  = require('gulp'),
      paths = require('./paths'),
      del   = require('del');

gulp.task('clean', function () {
    return del([paths.build.dist]);
});
