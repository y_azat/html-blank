'use strict';
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const paths = require('./paths');
//const emitty = require('emitty').setup(paths.src.base, 'pug');

gulp.task('watch-pug', function() {
    return $.watch(paths.src.pug, function(){
        gulp.start('pug');
    });
});

gulp.task('watch-styles', function() {
    return $.watch([paths.src.base + 'common/styles/**/*.styl', paths.src.base + 'components/**/*.styl', paths.src.base + 'pages/**/*.styl'], function(){
        gulp.start('styles');
    });
});

gulp.task('watch-js', function() {
    return $.watch(paths.src.base + '**/*.js', function(){
        gulp.start('webpack');
    });
});

gulp.task('watch-assets', function() {
    return $.watch(paths.src.assets + '**/*.*', function(){
        gulp.start('assets');
    });
});

gulp.task('watch-img', function() {
    return $.watch(paths.src.img + '**/*.{svg,png,jpg}', function(){
        gulp.start('imagemin');
    });
});

gulp.task('watch-sprite', function() {
    return $.watch(paths.src.img + 'sprite/*.*', function(){
        gulp.start('sprite');
    });
});

gulp.task('watch', ['watch-pug']);
