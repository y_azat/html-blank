const gulp  = require('gulp'),
      paths = require('./paths'),
      $     = require('gulp-load-plugins')();

gulp.task('imagemin', function () {
    return gulp.src(paths.src.img + '**/*.{svg,png,jpg}')
        .pipe($.newer(paths.build.img))
        .pipe($.imagemin({
            progressive:       true,
            optimizationLevel: 4
        }))
        .pipe(gulp.dest(paths.build.img));
});