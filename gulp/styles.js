/**
 * SCSS, minification
 */

const gulp  = require('gulp'),
      paths = require('./paths'),
      $     = require('gulp-load-plugins')(),
      isDev = require('./env.js');

gulp.task('styles', function () {
    return gulp
        .src(paths.src.styles)
        .pipe($.plumber())
        .pipe($.if(isDev, $.sourcemaps.init()))
        .pipe($.sass())
        .pipe($.autoprefixer("last 5 version", "> 1%", "Explorer >= 8", {
            cascade: true
        }))
        .pipe($.if(isDev, $.sourcemaps.write()))
        .pipe($.if(!isDev, $.csso()))
        .pipe($.rename("all.min.css"))
        .pipe(gulp.dest(paths.build.css));
});