/**
 * SCSS, minification
 */

const gulp    = require('gulp'),
      paths   = require('./paths'),
      $       = require('gulp-load-plugins')(),
      webpack = require('webpack-stream');

gulp.task('js', function () {
    return gulp.src(paths.src.js)
        .pipe($.plumber())
        .pipe(webpack(require('../webpack.config.js'), null))
        .pipe(gulp.dest(paths.build.js));
});