'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const paths = require('./paths');

gulp.task('serve', function() {
    browserSync.init({
        server: paths.build.dist,
        port: 3000

    });

    browserSync.watch(paths.build.dist + '**/*.*').on('change', browserSync.reload);
});