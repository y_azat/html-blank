/**
 * Webpack config
 */
const webpack        = require('webpack'),
      isDev          = require('./gulp/env.js'),

      UglifyJSPlugin = require('uglifyjs-webpack-plugin')

let config = {

    output: {
        filename: 'app.min.js'
    },

    devtool: isDev ? 'eval' : false,

    module:  {
        loaders: [
            {
                test:    /\.jsx?$/,
                exclude: /(node_modules|bower_components|vendor|vendors)/,
                use:     {
                    loader:  'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $:               'jquery',
            jQuery:          'jquery',
            'window.jQuery': 'jquery',
            'window.$':      'jquery'
        }),
    ]
}

if (!isDev) {
    config.plugins.push(
        new UglifyJSPlugin()
    )
}

module.exports = config;